// All rights reserved Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "ShooterCharacter.h"
#include "UObject/SoftObjectPtr.h"
#include "PlayerShooterCharacter.generated.h"

/**
 * 
 */
UCLASS()
class FPCODECXX_API APlayerShooterCharacter : public AShooterCharacter
{
	GENERATED_BODY()

public:
	APlayerShooterCharacter();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

protected:
	virtual void BeginPlay() override;

private:
	//TODO: Move this into the base ShooterCharacter class I guess.
#pragma region Weapons
	//This should be assigned in the UE editor which should be BP_<Whatever you created a BP from the class AGunBase>
	//NGL Rider helps a lot and will let you know if you have not attached anything here and if it is null.
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class AGunBase> HandledWeapon;

	class AGunBase* SpawnedWeapon;
#pragma endregion

#pragma region Character Configuration
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Configuration", meta = (AllowPrivateAccess = "true"))
	float TurnRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Configuration", meta = (AllowPrivateAccess = "true"))
	float LookRate;
#pragma endregion

#pragma region Input Configurations
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	TSoftObjectPtr<class UInputMappingContext> OnFootInputMapping;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* MoveForwardAction;
	void MoveForwardCallback(const struct FInputActionInstance& Instance);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* JumpAction;
	void JumpActionCallback(const struct FInputActionInstance& Instance);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* StrafeRightAction;
	void StrafeRightCallback(const struct FInputActionInstance& Instance);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* LookUpAction;
	void LookUpCallback(const struct FInputActionInstance& Instance);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* TurnAction;
	void TurnCharacterCallback(const struct FInputActionInstance& Instance);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Input Mappings", meta = (AllowPrivateAccess = "true"))
	class UInputAction* TriggerHoldDownAction;
	void TriggerHeldDownCallback(const struct FInputActionInstance& Instance);
#pragma endregion

#pragma region Camera Components
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArmComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComp;
#pragma endregion
};
