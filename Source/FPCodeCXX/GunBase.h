// All rights reserved Innocent Sign Inn

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GunBase.generated.h"

UCLASS()
class FPCODECXX_API AGunBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGunBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly)
	class USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(VisibleAnywhere)
	class USceneComponent* CustomRoot;

	void TriggerWeapon();

private:
	UPROPERTY(EditDefaultsOnly, meta=(AllowPrivateAccess), Category="VFX Configurations")
	class UParticleSystem* WeaponMuzzleFlash;

	UPROPERTY(EditAnywhere, meta=(AllowPrivateAccess), Category="Weapon Paramters")
	float MaxRange;
};
