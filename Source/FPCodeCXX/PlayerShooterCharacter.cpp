// All rights reserved Innocent Sign Inn


#include "PlayerShooterCharacter.h"
#include "GunBase.h"
#include "InputMappingContext.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "DrawDebugHelpers.h"

APlayerShooterCharacter::APlayerShooterCharacter(): AShooterCharacter()
{
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));

	SpringArmComp->SetupAttachment(RootComponent);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

	CameraComp->SetupAttachment(SpringArmComp);
}

void APlayerShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayerShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	//TODO A cleaner way to get the player controller???
	const ULocalPlayer* LocalPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	if (UEnhancedInputLocalPlayerSubsystem* InputSystem = LocalPlayer->GetSubsystem<
		UEnhancedInputLocalPlayerSubsystem>())
	{
		if (!OnFootInputMapping.IsNull())
		{
			InputSystem->AddMappingContext(OnFootInputMapping.LoadSynchronous(), 5000);
		}
	}

	SpawnedWeapon = GetWorld()->SpawnActor<AGunBase>(HandledWeapon);

	//To hide the existing weapon, maybe we could have replaced it instead?
	GetMesh()->HideBoneByName(TEXT("weapon_r"), PBO_None);

	//Lets create a Socket and attached the weapon we created, because why not.
	//And you need to learn sockets.

	SpawnedWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
	SpawnedWeapon->Owner = this;
}

void APlayerShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	UEnhancedInputComponent* Input = Cast<UEnhancedInputComponent>(PlayerInputComponent);
	Input->BindAction(MoveForwardAction, ETriggerEvent::Triggered, this, &APlayerShooterCharacter::MoveForwardCallback);
	Input->BindAction(StrafeRightAction, ETriggerEvent::Triggered, this, &APlayerShooterCharacter::StrafeRightCallback);
	Input->BindAction(LookUpAction, ETriggerEvent::Triggered, this, &APlayerShooterCharacter::LookUpCallback);
	Input->BindAction(TurnAction, ETriggerEvent::Triggered, this, &APlayerShooterCharacter::TurnCharacterCallback);
	Input->BindAction(JumpAction, ETriggerEvent::Triggered, this, &APlayerShooterCharacter::JumpActionCallback);
	Input->BindAction(TriggerHoldDownAction, ETriggerEvent::Triggered, this, &APlayerShooterCharacter::TriggerHeldDownCallback);
}

void APlayerShooterCharacter::JumpActionCallback(const FInputActionInstance& Instance)
{
	if (Instance.GetValue().Get<float>())
	{
		Jump();
	}
	else
	{
		StopJumping();
	}
}

void APlayerShooterCharacter::MoveForwardCallback(const FInputActionInstance& Instance)
{
	const float Val = Instance.GetValue().Get<float>();
	AddMovementInput(GetActorForwardVector(), Val);
}

void APlayerShooterCharacter::StrafeRightCallback(const FInputActionInstance& Instance)
{
	const float Val = Instance.GetValue().Get<float>();
	AddMovementInput(GetActorRightVector(), Val);
}

void APlayerShooterCharacter::LookUpCallback(const FInputActionInstance& Instance)
{
	const float Val = Instance.GetValue().Get<float>();
	AddControllerPitchInput(Val * LookRate);
}

void APlayerShooterCharacter::TurnCharacterCallback(const FInputActionInstance& Instance)
{
	const float Val = Instance.GetValue().Get<float>();
	AddControllerYawInput(Val * TurnRate);
}

void APlayerShooterCharacter::TriggerHeldDownCallback(const FInputActionInstance& Instance)
{
	if (const bool Val = Instance.GetValue().Get<bool>())
	{
		SpawnedWeapon->TriggerWeapon();
	}
}
