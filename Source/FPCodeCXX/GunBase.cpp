// All rights reserved Innocent Sign Inn


#include "GunBase.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AGunBase::AGunBase()
{
	PrimaryActorTick.bCanEverTick = true;
	CustomRoot =CreateDefaultSubobject<USceneComponent>(TEXT("Custom Root"));
	SetRootComponent(CustomRoot);
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon Mesh"));
	WeaponMesh->SetupAttachment(CustomRoot);
}

// Called when the game starts or when spawned
void AGunBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AGunBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGunBase::TriggerWeapon()
{
	UGameplayStatics::SpawnEmitterAttached(WeaponMuzzleFlash, WeaponMesh, TEXT("MuzzleFlashSocket"));
	APawn* OwnerPawn = GetOwner<APawn>();
	if (OwnerPawn != nullptr)
	{
		FVector location;
		FRotator rotation;
		OwnerPawn->Controller->GetPlayerViewPoint(location, rotation);
		FVector end = location + rotation.Vector() * MaxRange;
		FHitResult OutHit;
		if (GetWorld()->LineTraceSingleByChannel(OutHit, location, end, ECollisionChannel::ECC_GameTraceChannel1))
		{
			UE_LOG(LogTemp, Warning, TEXT("Hit the object %s"), *OutHit.GetActor()->GetName());
			DrawDebugLine(GetWorld(), location, OutHit.ImpactPoint, FColor::Red, true);
		}
	}
}

